---
title: Cryptographie et sécurité
author: PF Villard
geometry: margin=1.5cm
---

Table of Contents
- [Lien vers le cours](#lien-vers-le-cours)
- [Exercices de TP](#exercices-de-tp)
  - [Sécurité](#sécurité)
- [Syllabus](#syllabus)

# Lien vers le cours

- **Les slides de cours se trouvent [ici](https://seshat.gitlabpages.inria.fr/crypto)**

# Exercices de TP

## Sécurité
- [TP sur le chiffrement avec Gnu Privacy Guard](JupyterNotebook/Chiffrement_avec_GnuPGP.ipynb)
- [TP sur le stockage des mots de passes](JupyterNotebook/Stockage.ipynb)
- [TP sur le chiffrement Symétrique](JupyterNotebook/ChiffrementSym.ipynb)
- [TP sur le chiffrement Asymétrique](JupyterNotebook/ChiffrementAsym.ipynb)


# Syllabus

1. Sécurité <!-- omit in toc -->
    1. Principes de sécurité informatique <!-- omit in toc -->
        1.  Motivation : pourquoi besoin de sécurité
        2.  Menaces accidentelles et intentionnelles
        3.  Modèles de sécurité, authentification
    2. Stockage des mots de passes <!-- omit in toc -->
       1. Hashage
          - md5 
          - sha1,
          - sha256 / sha512
       2. Attaque par dictionnaire
       3. Attaque par force brute

2. Cryptographie <!-- omit in toc -->
   1. Principe de la cryptographie <!-- omit in toc -->
      - Définitions
      - Vocabulaire
   2. Age artisanal <!-- omit in toc -->
      - Scytale avec démo
      - Code de césar avec démo
      - Principe de substitution avancée
      - Cryptanalyste  Arabe
      - Chiffre de Marie Stuart
      - Chiffre de Végenère
      - Travaux de Babbage
    3. Age technique : mécanisation <!-- omit in toc -->
         - Code allemand ADFGVX
         - Système de Mauborgne    
         - Le disque à chiffrer de Alberti
         - Machine Enigma
    4. Age paradoxal <!-- omit in toc -->
         - Lucifer
         - Diffie-Hellman
         - RSA
         - PGP
         - GnuPG
         - OpenSSL
  