# Corrections TD

## 1 - Scytale

### 1.a

```
# Ecrire un texte
texte_a_chiffrer="Le BUT INFO est le nouveau diplome de référence des IUT"
# Nombre de coté de votre Scytale
rows = 6 
# Test pour vérifier le nombre de colonne
n = len(texte_a_chiffrer)
if (n % rows == 0):
    print("Texte chiffré :")
    # Appeler la commande pour chiffrer le texte
    texte_chiffre=encrypt(rows, texte_a_chiffrer)
    print(texte_chiffre)
else:
    print("taille du texte non conforme "+str(n)+" à la place d'un multiple de "+str(rows))
```

### 1.b

```
# Ecrire un texte
texte_a_dechiffrer="Liuietro a nBbf UlapTevr mooIerfNnieFtssO es crieo osnintcnn usev eleprlrote" 
# Nombre de coté de votre Scytale
rows = 4
# Test pour vérifier le nombre de colonne
n = len(texte_a_dechiffrer)
if (n % rows == 0):
    print("Texte déchiffré :")
    # Appeler la commande pour chiffrer le texte
    texte_dechiffre=decrypt(rows, texte_a_dechiffrer)
    print(texte_dechiffre)
else:
    print("taille du texte non conforme "+str(n)+" à la place d'un multiple de "+str(rows))
```

## 2 - Chiffre de César

### 2.a

```
num = 3 # Mettre le décalage
text= "Le cursus suivi en formation initiale prévoit deux stages professionnels"  #Entrer le texte à coder
ciphertext = encrypt_caesar(num, text)
print("Texte codé",ciphertext)
```

### 2.b

```
num = 7 # Mettre le décalage
text= "h s pzzbl kb iba slz kpwsvtlz wlbclua wvbyzbpcyl slbyz labklz la pualnyly slz mvythapvuz lu thzaly"  #Entrer le texte à décoder
ciphertext = decrypt_caesar(num, text)
print("texte décodé : ",ciphertext)
```

### 3.a

```
string = 'LEBUTENALTERNANCESEFFECTUEAPARTIRDELADEUXIEMEANNEE'
keyword = 'BUTINFO'
key = generateKey(string, keyword)
encrypt_text = encryption(string,key) 
print("Encrypted message:", encrypt_text) 
```

### 3.b

```
string = 'BXFQANGULTBRZFEYUIFJRFXHVAJST'
keyword = 'BUTINFO'
key = generateKey(string, keyword)
decrypt_text = decryption(string,key) 
print("Encrypted message:", decrypt_text) 
```

