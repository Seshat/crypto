## Objectif

- En TD :
    - Comprendre les besoins de sécurité
    - Connaître l'historique de la cryptographie
    - Manipuler des objets de cryptographie
    - Utiliser du code de cryptographie
- En TP
    - Chiffrer, déchiffrer et signer des messages avec GnuPG
    - Stockage des mots de passe
    - Chiffrement symétrique et asymétrique


# Securité

## Motivation <!-- omit in toc -->

- Besoins de sécurité **&nearr;**
- Matériel informatique omniprésent 
<!-- matériel accessible à prix très abordable
logiciels se simplifier (au niveau de l’utilisation !) et permettent une prise en main rapide. -->
- Entreprises nécessitent  réseau sécurisé pour transfert des données
<!-- entre les machines de cette entreprise, ou avec des machines externes, distantes de plusieurs milliers de kilomètres. -->
- Evolution constante des techniques
<!-- sécuriser l’échange de ces données ou  pour contourner ces systèmes sécurisés. -->
- Sécurité des données tend à s’améliorer
<!-- l’étude des contournements possibles est simultanée à l’étude des protections. La tendance actuelle veut que les résultats découverts, tous domaines confondus, soient publiés. Dans le cadre de la sécurité informatique, cela permet de découvrir plus rapidement les failles et/ou avantages de certaines techniques. -->

## Applications

- Carte bancaire
- Téléphonie mobile
- Messagerie (signal, telegram, WhatsApp, etc)
- Carte d’assuré social
- Monéo
- Email (protonmail, etc)
- ...

## Menaces <!-- omit in toc -->

- Les menaces accidentelles
<!-- aucune préméditation. Dans cette catégorie, sont repris les bugs logiciels, les pannes matérielles, et autres défaillances "incontrôlables". -->
- Les menaces intentionnelles :
    - passives 
    - actives
<!-- l’action d’un tiers désirant s’introduire et re- lever des informations. Dans le cas d’une attaque passive, l’intrus va tenter de dérober les informations par audit, ce qui rend sa détection relativement difficile. En effet, cet audit ne modifie pas les fichiers, ni n’altère les systèmes. Dans le cas d’une attaque active, la détection est facilitée, mais il peut être déjà trop tard lorsque celle-ci a lieu. Ici, l’intrus aura volontairement modifié les fichiers ou le système en place pour s’en emparer. -->

## Menaces intentionnelles <!-- omit in toc -->

- **Interruption** = problème lié à la disponibilité des données
- **Interception** = problème lié à la confidentialité des données
- **Modification** = problème lié à l’intégrité des données
- **Fabrication** = problème lié à l’authenticité des données

![](fig/im1.png){width=50%}

<!-- Les auteurs de ces attaques sont notamment les hackers (agissant souvent par défi personnel), les concurrents industriels (vol d’informations concernant la stratégie de l’entreprise ou la conception de projets), les espions, la presse ou encore les agences nationales. -->

## Modèles de sécurité <!-- omit in toc -->

- Modèle CIA
- Le protocole AAA
- Authentification forte

## Modèle CIA <!-- omit in toc -->

- **Confidentialité** : l’information n’est connue que des entités communicantes
- **Intégrité** : l’information n’a pas été modifiée entre sa création et son traitement (en ce compris un
éventuel transfert)
- **Disponibilité** : l’information est toujours accessible et ne peut être bloquée/perdue

![](fig/im2.png){width=30%}

## Le contrôle d’accès : Le protocole AAA <!-- omit in toc -->

1. **Identification** : Qui êtes-vous ?
2. **Authentification** : Prouvez-le !
3. **Autorisation** : Avez-vous les droits requis ? 
4. **Accounting/Audit** : Qu’avez-vous fait ?
   
![](fig/im3.png){width=50%}

<!-- On parle du protocole AAA (les deux premières étapes sont fusionnées). Dans certaines situations, on scindera la dernière étape. On parlera d’Accounting lorsque le fait de comptabiliser des faits sera demandé, et d’Audit lorsque des résultats plus globaux devront être étudiés.
Notons également que l’authentification, visant à prouver l’identité d’un individu peut se faire de plusieurs manières : -->

## Authentification forte <!-- omit in toc -->

- Ce que vous **savez** (mot de passe, code PIN, etc.)
- Ce que vous **avez** (carte magnétique, lecteur de carte, etc.) 
- Ce que vous **êtes** (empreintes digitales, réseau rétinien, etc.)

## Stockage des mots de passes <!-- omit in toc -->

- Hashage
    - md5 
    - sha1,
    - sha256 / sha512
- Attaque par dictionnaire
- Attaque par force brute

## Gnu Privacy Guard

- **GnuPG** est un outil qui permet de chiffrer, déchiffrer et signer des messages en utilisant les algorithmes de chiffrements

- Un certain nombre d'algorithmes de chiffrement et de hashage sont pris en charge par **GnuPG**

-> Nous allons voir les algo qui existent aujourd'hui

## Fonction de hashage<!-- omit in toc -->

- Mots de passe pas stockés directement sur fichier

- Hash des mot de passe enregistré

- **hash** = suite de caractères de taille fixe associée à une chaîne quelconque

## Propriétés <!-- omit in toc -->

- Difficile de trouver une chaîne ayant un hash donné
- Difficile de modifier une chaîne sans modifier son hash
- Difficile de trouver deux chaînes avec le même hash

## Algorithmes de hashage<!-- omit in toc -->

- md5 (mais cet algorithme n'est plus sûr)
- sha1,
- sha256 / sha512

-> Pour vous authentifier sur un site, vous tapez votre mot de passe, et le programme vérifie que son hash est bien identique au hash stocké sur le serveur

# Cryptographie

## Principes généraux (1/3) <!-- omit in toc -->

![](fig/AliceBob1.png){ width=900px }

<!-- <p style="font-size:10px">
D'après "The Gossip Chain" de Norman Rockwell
</p)> -->

<p style="font-size:10px">
D'après "The Gossip Chain" de Norman Rockwell
</p>

## Principes généraux (2/3) <!-- omit in toc -->

![](fig/AliceBob2.png){ width=900px }

<!-- <p style="font-size:10px">
D'après "The Gossip Chain" de Norman Rockwell
</p)> -->

<p style="font-size:10px">
D'après "The Gossip Chain" de Norman Rockwell
</p>


## Principes généraux (3/3) <!-- omit in toc -->

![](fig/AliceBob3.png){ width=900px }

<!-- <p style="font-size:10px">
D'après "The Gossip Chain" de Norman Rockwell
</p)> -->

<p style="font-size:10px">
D'après "The Gossip Chain" de Norman Rockwell
</p>

## Ordres de grandeur <!-- omit in toc -->

- Contre-mesure à l’attaque par « Brute Force » 
- Notion de grandeur
 - e.g. une clef aléatoire de 128 bits
  $2^128 \approx 3,4 10^38$
- Nombre de gouttes d’eau dans les océans
  $4,2 10^25$
- Nombre de grains de sables sur Terre
  $2 10^26$
- Nombre de molécules d’eau sur Terre
  $4,6 10^46$


## Garanties cryptographiques <!-- omit in toc -->

- Confidentialité
    - Autorisation d’accès pour l’accès au « secret »
    - Indépendant du partage du secret 
- Authenticité
  - Garantie de la légitimité pour l’autorisation d’accès
- Intégrité
  - Garantie de non-altération du secret lors de l’échange

## Age artisanal

- Steganographie
- Scytale
- Code de césar
- Chiffre de Végenère
- Chiffre de Marie Stuart

![](https://upload.wikimedia.org/wikipedia/commons/c/cb/1922-1-14-Boy-with-Stereoscope-Norman-Rockwell.jpg){ width=200px }

<p style="font-size:10px">
source: commons.wikimedia.org
</p>

## Steganographie

- **Steganos** : couvert
- **Graphein** : ecriture
- **&rarr;** Dissimuler l'exstence d'un message

## 
|![](https://upload.wikimedia.org/wikipedia/commons/8/84/Cropped-removebg-herodotus-historian.png)|![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/5c/POxy_v0017_n2099_a_01_hires.jpg/639px-POxy_v0017_n2099_a_01_hires.jpg)|
|-------------------------------------------------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------|
|    Hérodote       |    Histories      |


## 
![](fig/sparta.gif)

|![](fig/Gorgo_Sparta.jpeg)|![](fig/waxtablet.jpeg)|
|-----------------------------|--------------------------|
|Reine Gordot  |    Tablettes de          cire           |

## 

|       ![](fig/The-Greek-Secret-Tattoo-Messages.jpeg){ width=600px }        |
|-------------------------------------------------------------|
|![](fig/Detail-of-an-illustration-by-Giorgio-De-Gaspari.jpeg){ width=600px }|


## 
|![](fig/Giovanni-4-2.jpeg){ width=600px }|
|--------------------------|
| ![](fig/turn-tweet.png){ width=600px }  |

## Steganographie &rarr; Cryptographie

- **Kryptos** : caché
- **Graphein** : ecriture
- **&rarr;** ~~Dissimuler l'exstence~~ Cacher le contenu d'un message
  
- Stegano et Crypto peuvent être combinés **&rarr;** micropoint, microfilms 

![](https://upload.wikimedia.org/wikipedia/en/thumb/5/5c/JamesBondLogo.png/220px-JamesBondLogo.png)

## substitutions et Transpositions

- **Substitutions** : remplacer chaque lettre par une autre selon une règle convenue

```  

ABCDEFGHIKLMNOPQRSTVXYZ
↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓
DEFGHIKLMNOPQRSTVXYZABC
```

- **Transpositions** : l’ordre des lettres est changé de façon à aboutir à un mélange sans cohérence
  
```

TON SECRET EST TON PRISONNIER

T N E R T S T N R S N I R
 O S C E E T O P I O N E

TNERTSTNRSNIRIFITDVEDASNRSNIROSCEETOPIONE
```

## Scytale

- Un des plus anciens chiffrements de transposition ayant été utilisé.
-  **Plutarque** raconte son utilisation par Lysandre de Sparte en 404 av. J.-C.

```

_____________________________________________________________
       |   |   |   |   |   |  |
       | O | n | s | a | m |  |
     __| u | s | e | b | i |__|
    |  | e | n | e | n | c |
    |  | r | y | p | t | o |
    |  |   |   |   |   |   |
_____________________________________________________________
```

![](https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Skytale.png/260px-Skytale.png)

##

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Siege-alesia-vercingetorix-jules-cesar.jpg/1280px-Siege-alesia-vercingetorix-jules-cesar.jpg)


## Code de césar

![](fig/cesar.png){ width=100px }

|alphabet clair|  `a b c d e f g h i j k l m n o p q r s t u v w x y z`|
|----|------|
|alphabet chiffré| `d e f g h i j k l m n o p q r s t u v w x y z a b c` |
|texte clair| `cestcoollescoursdecrypto` |
|texte chiffré| `fhvwfrroohvfrxuvghfubswr` |

**&rarr; Combien de combinaison différentes ?**



## Principe de substitution avancée

- Alphabet chiffré disposant de toutes les lettres au hasard
- &rarr; Plus de 400 000 000 000 000 000 000 000 000 dispositions

|alphabet clair|  `a b c d e f g h i j k l m n o p q r s t u v w x y z`|
|----|------|
|alphabet chiffré| `d e f g h i j k l m n o p q r s t u v w x y z a b c` |
|texte clair| `cestcoollescoursdecrypto` |
|texte chiffré| `fhvwfrroohvfrxuvghfubswr` |

## 

![](https://upload.wikimedia.org/wikipedia/commons/3/39/Al-Kindi_Portrait.jpg)


## Cryptanalyste  Arabe

- **&rarr;** Décrypter un message sans connaître la clé
- IX ème siècle par Al Kindi
- **&rarr;** lien entre language et fréquence des lettres

<iframe src='https://fr.wikipedia.org/wiki/Fréquence_d%27apparition_des_lettres#Base_statistique_de_calcul_:_le_corpus' height="400" width="800" ></iframe>

## Exemple <!-- omit in toc -->

```
BAELXEJ JCKJ ZA JANBH HSXGXWXL XMXPJ LCEEA XK
GCP JGCPH DPOH. FKXEL AOOA AKJ JAGNPEA O’SPHJCPH LA NX’GKD OA HXMAJPAG,AOOA HA OAMX, YXPHX OA HCO LAMXEJ OA HCKMAGXPE AJ
OKP LPJ : "IGXEL GCP, LABKPH NPOOA AJ KEA EKPJH RA J’XP GXZCEJA
OAH OAIAELAH LAH GCPH BXHHAH BKPH-RA NA BAGNAJJGA LA
HCOOPZPJAG KEA DXMAKG LA MCJGA NXRAHJA ?"
ABPOCIKA-ZCEJAH LAH NPOOA AJ KEA EKPJH
```

## Occurrences <!-- omit in toc -->

 <font size="2">

|  Lettre   |Fréquence|  Lettre   |Fréquence |
|-----|-----------|---------|-----------|----------|
|-----|Occurrences| % |-----|Occurrences|    %     |
|A|61|19,5|N|9 |2,9 |
|B|8 |2.4 |O|23|7.3 |
|C|14|4.5 |P|26|8.3 |
|D|3 | 1  |Q|0 | 0  |
|E|19|6.1 |R|3 |1.0 |
|F|1 |0,3 |S|2 |0.6 |
|G|18|5.7 |T|0 | 0  |
|H|28|8,9 |U|0 | 0  |
|I|3 | 1  |V|0 | 0  |
|J|28|8.9 |W|1 |0.3 |
|K|16|5.1 |X|20|6,4 |
|L|15|4,8 |Y|3 | 1  |
|M|7 |2.2 |Z|6 |1.9 |

</font>

- les 3 lettres les plus fréquentes : `A`, `H` ou `J` &rarr; `a`, `e` ou `i`
- Occurrences avant ou après les autres lettres de l'alphabet
  
`OA AOOA, OAH`

**&rarr;** Que vaut `A`, `O` et `H`


## Comment résister à l’analyse des fréquences ? <!-- omit in toc -->

##

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Vigenere.jpg/220px-Vigenere.jpg)

## Chiffre de Végenère

![](https://upload.wikimedia.org/wikipedia/commons/thumb/1/1a/Vigenere.jpg/220px-Vigenere.jpg){ width=200px }

- Utilisation ~~d'un~~ de plusieurs alphabets de 26 lettres
- **&rarr;** nombre de décalage &ne; pour chaque lettre

|mot-clef| `NORMANROCKWELL` |
|----|------|
|texte clair| `CEQUEJEMAMUSEENCRYPTO` |
|texte chiffré| `PSHGEWVACWQWPPAQIKPGF` |

## Carre de Vegenere

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Vigen%C3%A8re_square_shading.svg/1200px-Vigen%C3%A8re_square_shading.svg.png){width=400px}
 
<font size=5>

|mot-clef|      `NORMANROCKWELLNORMANR` |
|----|------|
|texte clair|   `CEQUEJEMAMUSEENCRYPTO` |
|texte chiffré| `PSHGEWVACWQWPPAQIKPGF` |

</font>

## 
![](https://upload.wikimedia.org/wikipedia/commons/6/6b/Charles_Babbage_-_1860.jpg){width=400px}

## Travaux de Babbage

![](https://upload.wikimedia.org/wikipedia/commons/6/6b/Charles_Babbage_-_1860.jpg){width=200px}

- Recherche des répétitions de mots courts
- **&rarr;** Recherche de la clé

## exemple <!-- omit in toc -->

XAUNMEESYUEDTLLFGSNBWQUFXPQTYO 
RUTYIIN**UMQI**EULSMFAFXGUTYBXXAGB 
HMIFIIM**UMQI**DEKRIFRIRZQUHIENOOOIG 
RMLYETYOVQRYSIXEOKIYPY**OIGR**MLYE 
TYOVQRYSIXEOKIYPY**OIGR**FBWPIYRBQ 
URJIYEMJ**IGRY**KXYACPPQSPBVESIRZQR 
UFREDYJ**IGRY**KXBLOPJARNPUGEFBWMI 
LXMZSMZYXPNBPUMYZMEEFBUGENLRD 
EPBJXONQEZTMBWOEFIIPAHPPQBFLGDE 
MFWFAHQ

## 

- Régularités repérées : **UMQI**, **OIGR**, **IGRY**
- Périodicité : multiple de la longueur de la clé
- Fréquences 25 et 30, donc longueur clé = **5** 
- **&rarr;** Recherche du nombre d’occurrences tous les 5
- **&rarr;** Trouver la clé 
- **&rarr;** Décoder le message

## Age technique : mécanisation

- Le disque à chiffrer de Alberti
- Cylindre de Jefferson 
- Machine Enigma

![](https://upload.wikimedia.org/wikipedia/commons/a/aa/Perpetual_Motion_by_Norman_Rockwell.jpg){ width=200px }

<p style="font-size:10px">
source: commons.wikimedia.org
</p>


## Disque à chiffrer des Confédérés

![](fig/disk.png){ width=200px }

- Vient du XVème siècle
- inventé par Léon Alberti
- Utilisé pendant 5 siècles


**&rarr; Quel type de chiffrement ?**

## Exemple d'utilisation

- Décoder un **message chiffré** en utilisant une **clé**
- Le message est `sqfrdz sqfze sqds`
- La clé est `IUTSTDIE`
    - Positionner la lettre A intérieur en face de la lettre de la clé
    - Lire la lettre intérieur correspondante


##

<iframe src='https://inventwithpython.com/cipherwheel/' height="400" width="800" ></iframe>

## Cylindre de Jefferson 

![](https://upload.wikimedia.org/wikipedia/commons/4/40/Jefferson%27s_disk_cipher.jpg){ width=200px }

<table bgcolor="#f7f8ff" cellpadding="3" cellspacing="0" border="1" style="font-size: 50%; text-align: center; border: gray solid 1px; border-collapse: collapse;">

<tbody><tr bgcolor="#888888">
<td>A</td>
<td>O</td>
<td>K</td>
<td>U</td>
<td>Y</td>
<td>U</td>
<td>X</td>
<td>R</td>
<td>I</td>
<td>A</td>
<td>U</td>
<td>K</td>
<td>U</td>
<td>F</td>
<td>H</td>
<td>L</td>
<td>O</td>
<td>N</td>
<td>S</td>
<td>W</td>
<td>K</td>
<td>L</td>
<td>K</td>
<td>T</td>
<td>R</td>
<td>F</td>
<td>P
</td></tr>
<tr bgcolor="#AAAAAA">
<td>M</td>
<td>Z</td>
<td>N</td>
<td>C</td>
<td>S</td>
<td>K</td>
<td>Y</td>
<td>O</td>
<td>N</td>
<td>S</td>
<td>L</td>
<td>K</td>
<td>T</td>
<td>R</td>
<td>F</td>
<td>A</td>
<td>J</td>
<td>Q</td>
<td>Q</td>
<td>B</td>
<td>R</td>
<td>T</td>
<td>X</td>
<td>Y</td>
<td>U</td>
<td>K</td>
<td>A
</td></tr>
<tr bgcolor="#EEEEEE">
<td>T</td>
<td>H</td>
<td>O</td>
<td>M</td>
<td>A</td>
<td>S</td>
<td>J</td>
<td>E</td>
<td>F</td>
<td>F</td>
<td>E</td>
<td>R</td>
<td>S</td>
<td>O</td>
<td>N</td>
<td>S</td>
<td>W</td>
<td>H</td>
<td>E</td>
<td>E</td>
<td>L</td>
<td>C</td>
<td>I</td>
<td>P</td>
<td>H</td>
<td>E</td>
<td>R
</td></tr>
<tr bgcolor="#AAAAAA">
<td>V</td>
<td>R</td>
<td>E</td>
<td>U</td>
<td>Y</td>
<td>W</td>
<td>R</td>
<td>3</td>
<td>V</td>
<td>P</td>
<td>F</td>
<td>E</td>
<td>7</td>
<td>J</td>
<td>Y</td>
<td>I</td>
<td>L</td>
<td>D</td>
<td>D</td>
<td>O</td>
<td>T</td>
<td>L</td>
<td>H</td>
<td>E</td>
<td>I</td>
<td>V</td>
<td>O
</td></tr>
<tr bgcolor="#888888">
<td>C</td>
<td>E</td>
<td>P</td>
<td>K</td>
<td>Q</td>
<td>H</td>
<td>6</td>
<td>K</td>
<td>S</td>
<td>Z</td>
<td>G</td>
<td>F</td>
<td>B</td>
<td>V</td>
<td>X</td>
<td>Y</td>
<td>H</td>
<td>Z</td>
<td>J</td>
<td>P</td>
<td>S</td>
<td>J</td>
<td>G</td>
<td>W</td>
<td>M</td>
<td>S</td>
<td>8
</td></tr></tbody></table>
<!-- https://www.thingiverse.com/thing:2123358 -->



## Machine Enigma

![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/31/Enigma-IMG_0484-black.jpg/220px-Enigma-IMG_0484-black.jpg)

- Les connexions échangent des paires de lettres
- Chaque rotor effectue une substitution
- L’ordre des rotors peut être changé 
- 1er tourne d’un cran après passage d’une lettre, 2ème quand 1er a fait un tour complet, etc. **&rarr;** 263=17 576 possibilités
- Pour 3 rotors **&rarr;**  6x17 576 = 105 456 possibilités
- Réflecteur : échange de paires de lettres

## Principe de la machine Enigma

![](https://upload.wikimedia.org/wikipedia/commons/thumb/6/6c/Enigma-action.svg/1200px-Enigma-action.svg.png){ width=400px }


## 

<iframe src='https://www.101computing.net/enigma/enigma-M3.html' height="800" width="1000" ></iframe>


##

![](https://upload.wikimedia.org/wikipedia/commons/1/17/Alan_Turing_%281912-1954%29_in_1936_at_Princeton_University.jpg){ width=400px }

## Déchiffrement d'Enigma

- Compter sur la rigueur allemande (ex : météo **&rarr;** `Wetter` à 6h05)
- Essai par mots probables (ex : `Wetter` **&rarr;** `ETJWPX`)
- Simplifications du problème grâce à des recherches antérieures polonaises
- Utilisation de *machines mathématiques*

![](https://upload.wikimedia.org/wikipedia/commons/thumb/7/7a/Wartime_picture_of_a_Bletchley_Park_Bombe.jpg/220px-Wartime_picture_of_a_Bletchley_Park_Bombe.jpg)


##   Age paradoxal 

- Lucifer
- Diffie-Hellman
- RSA
- PGP
- GnuPG
- OpenSSL

![](https://upload.wikimedia.org/wikipedia/commons/c/c4/Saturday_Evening_Post_1922-05-20.jpg){ width=200px }

<p style="font-size:10px">
source: commons.wikimedia.org
</p>


## pression des milieux économiques <!-- omit in toc -->

- Sécuriser les échanges commerciaux
- Organiser les relations entre public et privé 
    - au plan collectif
    - Dans le code de transmission des messages
- Faire passer ces questions dans le domaine public c-à-d. abandon du secret défense

## Exemple : données bancaires <!-- omit in toc -->

<section data-markdown>
<script type="text/template">
  <div class="r-stack">
![](fig/bank/toto_6.png) <!-- .element: class="fragment fade-in-then-out" data-fragment-index="1" -->
![](fig/bank/toto_5.png) <!-- .element: class="fragment fade-in-then-out" data-fragment-index="2" -->
![](fig/bank/toto_4.png) <!-- .element: class="fragment fade-in-then-out" data-fragment-index="3" --> 
![](fig/bank/toto_3.png) <!-- .element: class="fragment fade-in-then-out" data-fragment-index="4" -->
![](fig/bank/toto_2.png) <!-- .element: class="fragment fade-in-then-out" data-fragment-index="5" -->
![](fig/bank/toto_1.png) <!-- .element: class="fragment fade-in-then-out" data-fragment-index="6" --> 
</div>
</script>  
</section>


## Passage à un calculateur <!-- omit in toc -->

|Lettre| Code  |Lettre| Code  |Lettre|  Code  |
|------|-------|------|-------|------|--------|
|  a   |1100001|  j   |1101010|  s   |1110011 |
|  b   |1100010|  k   |1101011|  t   |1110100 |
|  c   |1100011|  l   |1101100|  u   |1110101 |
|  d   |1100100|  m   |1101101|  v   |1110110 |
|  e   |1100101|  n   |1101110|  w   |1110111 |
|  f   |1100110|  o   |1101111|  x   |1111000 |
|  g   |1100111|  p   |1110000|  y   |1111001 |
|  h   |1101000|  q   |1110001|  z   |1111010 |
|  i   |1101001|  r   |1110010|     |       |


## Mécanisme à clé symétrique <!-- omit in toc -->

## Exemple <!-- omit in toc -->

IUT **&rarr;** 1101001 1110101 1110100

- Transposition 

```

11 01 00 11 11 01 01 11 10 10 0
11 10 00 11 11 10 10 11 01 01 0 
```

1110001 1111010 1101010**&rarr;** QZJ

- Substitution avec Clé DIE

```

Texte clair   : 1101001 1110101 1110100
clef DIE      : 1100100 1101001 1100101
Texte chiffré : 0001101 0011100 0010001
```

## Utilisation du Ou exclusif

<table border="1" cellspacing="0">

<tbody><tr style="background:#b3e2d1;text-align:center">
<td colspan="3">Table de vérité de XOR
</td></tr>
<tr style="text-align:center">
<td width="40"><b>A</b></td>
<td width="40"><b>B</b></td>
<td width="100"><b>R = A ⊕ B</b>
</td></tr>
<tr style="text-align:center">
<td>0</td>
<td>0</td>
<td>0
</td></tr>
<tr style="text-align:center">
<td>0</td>
<td>1</td>
<td>1
</td></tr>
<tr style="text-align:center">
<td>1</td>
<td>0</td>
<td>1
</td></tr>
<tr style="text-align:center">
<td>1</td>
<td>1</td>
<td>0
</td></tr></tbody></table>


```

Texte clair   : 1101001 1110101 1110100
clef DIE      : 1100100 1101001 1100101
Texte chiffré : 0001101 0011100 0010001

Texte chiffré : 0001101 0011100 0010001
clef DIE      : 1100100 1101001 1100101
Texte clair   : 1101001 1110101 1110100
```

## Principe

On distingue deux types d'algorithmes :

- les algorithmes en blocs, qui prennent **`n`** bits en entrée et en ressortent 
**`n`**
- les algorithmes à flots, qui chiffrent bit par bit


<!-- ## Lucifer

- Inventé par Horst Feistel pour IBM -->

## DES <!-- omit in toc -->

- DES 1977 Data Encryption Standard
    - Niveau de sécurité lié à la clé : secrète
    - Algorithme public : Clair : 64 bits, découpé en 2 blocs, G et D, et un des blocs combiné avec une fonction qui utilise la clé. Itération du procédé.


## Du DES à l’AES <!-- omit in toc -->
- 1997 : DES cassé en 3 semaines par des ordinateurs effectuant des calculs en parallèle
- Triple DES
- AES Advanced Encryption Standard
    -  Appel d’offres international. 1997. Clair : 128 bits
    -  2001

## Avantages

- Raisonnablement facile
- Rapide à exécuter
- -> Sécuriser de grandes quantités de données


Dans la plupart des situations, la taille normale d'un secret de sécurité symétrique est de 128 ou 256 bits


##

- Comment éviter l’échange de clés ?
- Comment assurer la confidentialité ?

## Mécanisme à clé assymétrique <!-- omit in toc -->

## Principe du double cadenas<!-- omit in toc -->

![](fig/cadenas/toto1.png)

## Principe du double cadenas<!-- omit in toc -->

![](fig/cadenas/toto2.png) 

## Principe du double cadenas<!-- omit in toc -->

![](fig/cadenas/toto3.png) 

## Principe du double cadenas<!-- omit in toc -->

![](fig/cadenas/toto4.png)

## Principe du double cadenas<!-- omit in toc -->

![](fig/cadenas/toto5.png) 

## Principe du double cadenas<!-- omit in toc -->

![](fig/cadenas/toto6.png) 
 
## Principe du double cadenas<!-- omit in toc -->

![](fig/cadenas/toto7.png) 
 
## Ordre du chiffrement<!-- omit in toc -->

clef d'Alice :	
```
   a b c d e f g h i j k l m n o p q r s t u v w x y z
   H S F U G T A K V D E O Y J B P N X W C Q R I M Z L
```

Clef de Bernard : 
```
   a b c d e f g h i j k l m n o p q r s t u v w x y z
   C P M G A T N O J E F W I Q B U R Y H X S D Z K L V
```

|Message : |`vois  moi  à  midi`|
|----|-----|
|Crypté avec la clef d'Alice:|`RBVW YBV H YVUV` |
|Crypté avec la clef de Bernard:|`YPDZ LPD O LDSD` |
|Décrypté avec la clef d'Alice:|`MPJY ZPJ L ZJCJ` |
|Décrypté avec la clef de Bernard:|`cbir wbi y wiai`|


## Inspiration par les 2 cadenas  <!-- omit in toc -->

- Trouver une fonction ```f: nombre -> autre nombre``` 
- Irréversible (ex : peinture, casser oeuf) &ne; doubler
- -> Arithmétique modulo 

![](fig/mod7_clock.png)

- **2 + 3 ?**
- **2 + 6 ?**
- **10h maintenant et prochain cours dans 6h ?** 

## Arithmétique modulo 

```2 + 3 = 5 (mod 7)``` et ```2 + 6 = 1 (mod 7)```

En math : utilisation du reste de la division
```

11 x 9 = 99
        99 : 13 = 7, reste 8
11 x 9 = 8 (mod 13)
```
-> **Irréversible !**

## Nombres premiers

![](https://upload.wikimedia.org/wikipedia/commons/b/b9/Sieve_of_Eratosthenes_animation.gif){ width=400px }

$$
35 = 3 \times 7\\
77 = 7 \times 11\\
9991 = 103 \times 97 \\
x^2-y^2=(x+y)(x-y)
$$



## Principe

- Exemple de clé publique : [Amazon](https://amazon.com)
- -> On utilise la clé publique pour chiffrer
- **Amazon** Utilise sa clé privée pour déchiffrer 
- Clé symétrique : 62, 128 ou 256 bits
- -> Peut être plus longue que 1024 pour clé publique


## La cryptographie à clé publique  <!-- omit in toc -->

- La cryptographie à clé publique Le principe du double cadenas

  - A : Clé de chiffrement : publique 
  - B. Clé de déchiffrement : secrète
- Le chiffrement est une fonction à sens unique


## Le système à clé publique RSA

- Théorie des nombres et double échange de clés
- Idée :
```
Multiplier 2 très grands nombres premiers est une
opération facile p = 47, q = 71, module N = 3337. Retrouver les facteurs à partir du résultat est une opération très difficile.
```
  - Clé de chiffrement : un nombre C premier avec N
  - Clé de déchiffrement : son inverse multiplicatif dans un calcul (modulo)


## Principe

- Chiffrement

$$
C=m^e (mod \  N)
$$

**m**  message, **C**  texte chiffré, **e** clé publique, **N** autre clé publique

- Déchiffrement

$$
m=C^d (mod \ N)
$$

**d** clé privée

**N** est fait à partir de 2 nombres premiers **p** et **q**

## Résolution

$$
C=m^e (mod \ N)
$$

**Pour un hacker, ou est l'inconnu de l'equation ?**

## Seule solution

- Trouver la factorisation en nombre premier
- Mais **N** est très grand

## Génération des clés

1. Prendre des nombres premiers **p** et **q** très grands 
$$
N=pq
$$
2. Résoudre la fonction d'Euler
$$
\Phi(N)=(p-1)(q-1)
$$
-> Effacer **p** et **q**
3. Générer aléatoirement **e** tel que :
$$
1<e<\Phi(N)
$$

##

Pour la clé privée, trouver **d** tel que :
$$
d=e-1 (mod \ \Phi(N))\\
ed=1 (mod \ \Phi(N))
$$

**d** plus petit que $\Phi(N)$ mais plus grand que **q** et **p**


## Le logiciel semi-libre PGP

- 1991, Philip Zimmermann
- = Pretty Good Privacy
- Système hybride de sécurisation, utilisé pour les communications en ligne dans certaines entreprises ou institutions où les communications sont
« sensibles »
  - Chiffrement symétrique (clé privée) et asymétrique (clé publique)


# Exercices de TD

## Exercices de TD  <!-- omit in toc -->
Ils se trouvent [ici](JupyterNotebook/codesSecrets.ipynb)

*(à lire avec Jupyter Notebook ou Visual Studio Code)*

![](https://upload.wikimedia.org/wikipedia/commons/thumb/9/9a/Visual_Studio_Code_1.35_icon.svg/75px-Visual_Studio_Code_1.35_icon.svg.png) ![](https://upload.wikimedia.org/wikipedia/commons/thumb/3/38/Jupyter_logo.svg/120px-Jupyter_logo.svg.png){ width=70px }

 

