---
title: Cryptographie et sécurité
author: PF Villard
geometry: margin=1.5cm
---

# Sécurité

## Principes de sécurité informatique

- Motivation : pourquoi besoin de sécurité
- Menaces accidentelles et intentionnelles
- Modèles de sécurité, authentification
  
## Stockage des mots de passes

- Hashage
    - md5 
    - sha1,
    - sha256 / sha512
- Attaque par dictionnaire
- Attaque par force brute

# Cryptographie

## Principe de la cryptographie

- Définitions
- Vocabulaire

## Age artisanal

- Scytale avec démo
- Code de césar avec démo
- Principe de substitution avancée
- Cryptanalyste  Arabe
- Chiffre de Marie Stuart
- Chiffre de Végenère
- Travaux de Babbage


## Age technique : mécanisation

- Code allemand ADFGVX
- Système de Mauborgne
- Le disque à chiffrer de Alberti
- Machine Enigma

##   Age paradoxal 

- Lucifer
- Diffie-Hellman
- RSA
- PGP
- GnuPG
- OpenSSL
  




